﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiAgenda.Entities;

namespace ApiAgenda.Entities
{
    public class DBContext:DbContext
    {
        public DBContext(DbContextOptions<DBContext>options):base(options)
        {
        }

        public DbSet<Personas> Personas
        {
            get;
            set;
        }
    }
}
