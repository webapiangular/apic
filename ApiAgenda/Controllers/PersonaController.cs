﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ApiAgenda.Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ApiAgenda.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class PersonaController : Controller
    {
        private readonly DBContext context;

        public PersonaController(DBContext context)
        {
            this.context = context;
        }

        //Todas las personas
        [HttpGet]
        public IEnumerable<Personas> Get()
        {
            return context.Personas;
        }

        [HttpGet("{id}")]
        public Personas Get(int id)
        {
            var persona = context.Personas.FirstOrDefault(p => p.Id == id);
            return persona;
        }

        [HttpPost]
        public ActionResult Post([FromBody]Personas persona)
        {
            try
            {
                context.Personas.Add(persona);
                context.SaveChanges();
                return Ok();
            }catch(Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody]Personas persona)
        {            
            if (persona.Id == id)
            {
                context.Entry(persona).State = EntityState.Modified;
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var persona = context.Personas.FirstOrDefault(p => p.Id == id);            
            if (persona != null)
            {
                context.Personas.Remove(persona);
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

    }
}
